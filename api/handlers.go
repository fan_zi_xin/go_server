package main

import (
	"encoding/json"
	"github.com/fanfansun/go_video_server/api/dbops"
	"github.com/fanfansun/go_video_server/api/defs"
	"github.com/fanfansun/go_video_server/api/session"
	"io"

	//"github.com/fanfansun/go_video_server/api/dbops"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
)

func CreateUser(w http.ResponseWriter, R *http.Request, _ httprouter.Params) {
	//_, err := io.WriteString(w, "Crete User Handler")
	//if err != nil {
	//	fmt.Printf("注册模块异常%s\n", err)
	//	return
	//}
	res, _ := ioutil.ReadAll(R.Body)
	ubody := &defs.UserCredential{}

	if err := json.Unmarshal(res, ubody); err != nil {
		sendErrorResponse(w, defs.ErrorRequestBodyParseFailed)
		return
	}
	if err := dbops.AddUserCredential(ubody.Username, ubody.Pwd); err != nil {
		sendErrorResponse(w, defs.ErrorDBError)
	}

	id := session.GenerateNewSessionId(ubody.Username)
	su := &defs.SignedUp{Success: true, SessionId: id}

	if resp, err := json.Marshal(su); err != nil {
		sendErrorResponse(w, defs.ErrorInternalFaults)
		return
	} else {
		sendNormalResponse(w, string(resp), 201)
	}

}

func Login(w http.ResponseWriter, _ *http.Request, param httprouter.Params) {
	//rqUsername := param.ByName("user_name")
	//_, err := io.WriteString(w, rqUsername)
	//if err != nil {
	//	fmt.Printf("登录模块异常%s\n", err)
	//	return
	//}
	uname := param.ByName("user_name")
	io.WriteString(w, uname)
}
